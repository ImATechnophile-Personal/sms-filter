#! /usr/bin/python
from config import SQLALCHEMY_DATABASE_URI
from app import app, db
if __name__ == '__main__':
	db.create_all()
	db.session.commit()
