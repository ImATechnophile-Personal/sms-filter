import os
from flask import Flask, redirect, url_for, render_template, flash
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.config.from_object('config')
app.secret_key = '23467sdfghj@#$%^some_secret+_)(*^%$#ASDFGHJ'
db = SQLAlchemy(app)
from app import views, models
