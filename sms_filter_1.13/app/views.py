# -*- coding: utf-8 -*-
from flask import render_template, request, session, abort,flash, redirect, url_for, jsonify
from app import app, db
from app.models import Categories,Keywords,Userkeywords
from datetime import datetime
from dateutil.parser import parse
import os
import requests
import json
@app.route('/')
def filter():
	return render_template('index.html')
@app.route('/simple/dashboard')
def dashboard():
	return render_template('dashboard.html')
@app.route('/blacklist',methods=['GET', 'POST'])
def blacklist():
	if request.method == 'POST':
		keyword=request.form['keyword']
		obj= Keywords.query.filter_by(name=keyword).first()
		if not keyword :
			flash('Please enter the keywords', 'error')
			return redirect(url_for('dashboard'))
		if not obj:
			obj_1=Keywords(categories_id=1,name=keyword,description="",is_blocklist=1,is_active= 1,created_at=datetime.utcnow(),updated_at=datetime.utcnow())
			db.session.add(obj_1)
			db.session.commit()
			flash('Successfully blacklisted')
			return redirect(url_for('dashboard'))
		elif obj.is_blocklist==0 and obj.is_active==0:
			obj.is_blocklist=1
			obj.is_active=1
			obj.updated_at=datetime.utcnow()
			db.session.commit()
			flash('Already deleted key from whitelist is now successfully blocklisted')
			return redirect(url_for('dashboard'))
		elif obj.is_blocklist==0:
			obj.is_blocklist=1
			obj.updated_at=datetime.utcnow()
			db.session.commit()
			flash('Already whitelisted keyword is now successfully blocklisted')
			return redirect(url_for('dashboard'))
		elif obj.is_blocklist==1 and obj.is_active==0:
			obj.is_active=1
			obj.updated_at=datetime.utcnow()
			db.session.commit()
			flash('Already deleted keyword from blocklist is now successfully blocklisted')
			return redirect(url_for('dashboard'))
		flash('This keyword is already blocklisted')
		return redirect(url_for('dashboard'))
	return redirect(url_for('dashboard'))
@app.route('/whitelist',methods=['GET', 'POST'])
def whitelist():
	if request.method == 'POST':
		keyword=request.form['keyword']
		obj= Keywords.query.filter_by(name=keyword).first()
		if not keyword :
			flash('Please enter the keywords', 'error')
			return redirect(url_for('dashboard'))
		if not obj:
			obj_1=Keywords(categories_id=1,name=keyword,description="",is_blocklist=0,is_active= 1,created_at=datetime.utcnow(),updated_at=datetime.utcnow())
			db.session.add(obj_1)
			db.session.commit()
			flash('Successfully whitelisted')
			return redirect(url_for('dashboard'))
		elif obj.is_blocklist==1  and obj.is_active==0:
			obj.is_blocklist=0
			obj.is_active=1
			obj.updated_at=datetime.utcnow()
			db.session.commit()
			flash('Already deleted keyword from blocklist is now successfully whitelisted')
			return redirect(url_for('dashboard'))
		elif obj.is_blocklist==1:
			obj.is_blocklist=0
			obj.updated_at=datetime.utcnow()
			db.session.commit()
			flash('Already blocklisted keyword is now successfully whitelisted')
			return redirect(url_for('dashboard'))
		elif obj.is_blocklist==0 and obj.is_active==0:
			obj.is_active=1
			obj.updated_at=datetime.utcnow()
			db.session.commit()
			flash('Already deleted keyword from whitelist is now successfully whitelisted')
			return redirect(url_for('dashboard'))
		flash('This keyword is already whitelisted')
		return redirect(url_for('dashboard'))
	return redirect(url_for('dashboard'))
@app.route('/remove',methods=['GET', 'POST'])
def remove():
	if request.method == 'POST':
		keyword=request.form['keyword']
		obj= Keywords.query.filter_by(name=keyword).first()
		if not keyword :
			flash('Please enter the keywords', 'error')
			return redirect(url_for('dashboard'))
		if not obj:
			flash('This keyword does not exit to remove')
			return redirect(url_for('dashboard'))
		elif obj.is_active==0:
			flash('This keyword already removed')
			return redirect(url_for('dashboard'))
		elif obj.is_blocklist==1 or obj.is_blocklist==0:
			obj.is_active=0
			obj.updated_at=datetime.utcnow()
			db.session.commit()
			flash('Successfully removed')
			return redirect(url_for('dashboard'))
		
	return redirect(url_for('dashboard'))
@app.route('/json', methods=['GET', 'POST'])
def greater():
        date = request.args.get('date')
        c=request.url
        r=requests.post('https://requestb.in/1jej8zt1',data=c)
        black={}
        if not date:
                black['status']=not date
                black['blocklist']= [{'id':key.id,'name':key.name,'created_at':str(key.created_at),'updated_at':str(key.updated_at)} for key in Keywords.query.filter(Keywords.is_blocklist==1, Keywords.is_active ==1).all()]
                black['whitelist']= [{'id':key.id,'name':key.name,'created_at':str(key.created_at),'updated_at':str(key.updated_at)} for key in Keywords.query.filter(Keywords.is_blocklist==0, Keywords.is_active ==1).all()]
                black['deleted_keys']= [{'id':key.id,'name':key.name,'created_at':str(key.created_at),'updated_at':str(key.updated_at)} for key in Keywords.query.filter(Keywords.is_active ==0).all()]
                return jsonify(black)
        black['status']=not type(parse(date))== unicode
        black['blocklist']= [{'id':key.id,'name':key.name,'created_at':str(key.created_at),'updated_at':str(key.updated_at)} for key in Keywords.query.filter(Keywords.is_blocklist==1, Keywords.is_active ==1, Keywords.updated_at>parse(date)).all()]
        black['whitelist']= [{'id':key.id,'name':key.name,'created_at':str(key.created_at),'updated_at':str(key.updated_at)} for key in Keywords.query.filter(Keywords.is_blocklist==0, Keywords.is_active ==1, Keywords.updated_at>parse(date)).all()]
        black['deleted_keys']= [{'id':key.id,'name':key.name,'deleted_at':str(key.updated_at)} for key in Keywords.query.filter(Keywords.is_active ==0, Keywords.updated_at>parse(date)).all()]
        return jsonify(black)
@app.route('/user', methods=['GET', 'POST'])
def ukey():
	if request.method == 'POST':
		user_data=request.get_json()
		d=request.url
		r=requests.post('https://requestb.in/1jej8zt1',data=d)
		dic={}
		if user_data:
			user_id = user_data['user_id']
			country_code = user_data['country_code']
			keyword_list = user_data['keyword_list']
			for key in keyword_list:
				value=key['keyword']
				name=value.encode('utf-8')
				obj=Userkeywords(user_id=user_id,name=name,description="",country_code=country_code,is_blocklist=key['filter_type'],is_active=1,created_at=datetime.utcnow(),updated_at=datetime.utcnow()) 
				db.session.add(obj)
				db.session.commit()
			dic['status']=True
			dic['message']="keywords are successfully added in the server"
			return jsonify(dic)
		dic['status']=False
		dic['message']="Error,user details are not added in the server"
		return jsonify(dic)
	

               

