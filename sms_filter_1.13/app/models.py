from app import db
from sqlalchemy.dialects.mysql import TIMESTAMP
from datetime import datetime
from sqlalchemy.sql import expression
from flask_login import UserMixin
class Categories(UserMixin,db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(500),nullable=False,unique=True)
  description = db.Column(db.String(1000),nullable=True)
  is_active = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
  created_at = db.Column(db.DateTime,default=datetime.utcnow())
  updated_at = db.Column(db.DateTime,default=datetime.utcnow())
  keywords = db.relationship('Keywords', backref='Categories',
                                lazy='dynamic')

  def __init__(self, name, description, is_active, created_at, updated_at):
   self.name = name
   self.description = description
   self.is_active = is_active
   self.created_at = created_at
   self.updated_at = updated_at
  
class Keywords(UserMixin,db.Model):
  id = db.Column(db.Integer, primary_key=True)
  categories_id= db.Column(db.Integer, db.ForeignKey('categories.id'))
  name = db.Column(db.Unicode(500, collation='utf8mb4'),nullable=False,unique=True)
  description = db.Column(db.Unicode(1000,collation='utf8mb4'),nullable=True)
  is_blocklist = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
  is_active = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
  created_at = db.Column(db.DateTime,default=datetime.utcnow())
  updated_at = db.Column(db.DateTime,default=datetime.utcnow())
  
  def __init__(self,categories_id,name,description,is_blocklist,is_active,created_at,updated_at):
   self.categories_id= categories_id
   self.name = name
   self.description= description
   self.is_blocklist= is_blocklist
   self.is_active= is_active
   self.created_at= created_at
   self.updated_at= updated_at
 

class Userkeywords(UserMixin,db.Model):
  id = db.Column(db.Integer, primary_key=True)
  user_id= db.Column(db.String(500),nullable=False)
  name = db.Column(db.String(500),nullable=False)
  description = db.Column(db.String(1000),nullable=True)
  country_code=db.Column(db.String(500),nullable=False)
  is_blocklist = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
  is_active = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
  created_at = db.Column(db.DateTime,default=datetime.utcnow())
  updated_at = db.Column(db.DateTime,default=datetime.utcnow())
 
  def __init__(self,user_id,name,description,country_code,is_blocklist,is_active,created_at,updated_at):
   self.user_id= user_id
   self.name = name
   self.description= description
   self.country_code=country_code
   self.is_blocklist=is_blocklist
   self.is_active= is_active
   self.created_at= created_at
   self.updated_at= updated_at
   
   